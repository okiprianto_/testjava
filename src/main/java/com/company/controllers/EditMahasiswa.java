package com.company.controllers;

import com.company.model.MahasiswaDB;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

public class EditMahasiswa extends HttpServlet {
    MahasiswaDB mb = new MahasiswaDB();
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            Map where = new HashMap();
            where.put("id",request.getParameter("id"));
            ResultSet results = mb.get_where("mahasiswa",where);
            request.setAttribute("val",results);
            RequestDispatcher r = request.getRequestDispatcher("editform.jsp");
            r.forward(request,response);
        }catch (Exception e){
            System.out.println(e);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Map where = new HashMap();
        where.put("id",Integer.parseInt(request.getParameter("id")));

        Map data = new HashMap();
        data.put("fullname",request.getParameter("fullname"));
        data.put("address",request.getParameter("address"));
        data.put("status",request.getParameter("status"));
        data.put("gradesphysics",Integer.parseInt(request.getParameter("gradesphysics")));
        data.put("gradescalculus",Integer.parseInt(request.getParameter("gradescalculus")));
        data.put("gradesbiologi",Integer.parseInt(request.getParameter("gradesbiologi")));

        mb.update("mahasiswa",data,where);
        response.sendRedirect("/Tugas1/TampilMahasiswa");
    }
}

